/**
 * 
 */
package lwt.lab.beans;

/**
 * @author Robert
 *
 */
public class Order {
    private String productName;
    private String quantity;
    private String price;
    private double totalValue;

    /**
     * @return the productName
     */
    public String getProductName() {
	return productName;
    }

    /**
     * @param productName
     *            the productName to set
     */
    public void setProductName(String productName) {
	this.productName = productName;
    }

    /**
     * @return the quantity
     */
    public String getQuantity() {
	return quantity;
    }

    /**
     * @param quantity
     *            the quantity to set
     */
    public void setQuantity(String quantity) {
	this.quantity = quantity;
    }

    /**
     * @return the price
     */
    public String getPrice() {
	return price;
    }

    /**
     * @param price
     *            the price to set
     */
    public void setPrice(String price) {
	this.price = price;
    }

    /**
     * @return the totalValue
     */
    public double getTotalValue() {
	return totalValue;
    }

    /**
     * @param totalValue
     *            the totalValue to set
     */
    public void setTotalValue(double totalValue) {
	this.totalValue = totalValue;
    }

}
